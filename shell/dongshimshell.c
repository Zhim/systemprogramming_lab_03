/* dong shim shell */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define BUFF_SIZE 256
#define ARGV_SIZE 50
int getArgs(char *cmd, char **argv);
main(){
    char buf[BUFF_SIZE];
    char *argv[ARGV_SIZE];
    int narg;
    pid_t pid;
    while (1){
        printf("dongshim : >> ");
        gets(buf);
        narg = getArgs(buf, argv);
        
        pid=fork();
       
        if (pid == 0 ){
            if(strcmp(buf,"exit")) exit(0);
            execvp(argv[0],argv);
        }else if(pid > 0){
            wait((int * ) 0 );
        }else{
            perror("fork failed\n");
        }
    }
}

int getArgs(char *cmd, char **argv){
    int narg = 0; // number of argv

    while(*cmd){ // buf has command
        if (*cmd == ' '|| *cmd == '\t'){
            *cmd++ = '\0';      
          
         }else{
             argv[narg++] = cmd++;
             while(*cmd != '\0' && *cmd != ' ' && *cmd != '\t'){
                 cmd++;
             }
         }
    }
    argv[narg] = NULL;
    return narg;
}
